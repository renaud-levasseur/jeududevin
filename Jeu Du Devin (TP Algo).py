'''
Jeu du Devin
'''

'''
Raffinage:

(Côté Machine User)

-R0: Faire deviner un nombre choisit par l'ordinateur à l'utilisateur comris entre 1 et 999.

-R1:Comment faire choisir un nombre compris entre 1 et 999 à la machine ?
            on utilisera un module random(randint) compris entre 1 et 999 qui sera stocké dans une variable.
                    choix_num_machine= randint(1,999)
            
            
    Comment faire pour que l'user choissise un chiffre entre 1 et 999 ?
            la machine demandera de choisir un chiffre compris entre 1 et 999
                    choix_num_user= input("Veuillez choisir un chiffre compris entre 1 et 999")
    
                        
            
-R2:Comment faire pour que l'user trouve le chiffre choisit par la machine
        l'user continuera à soumettre un chiffre choisit tant qu'il n'aura pas trouvé le chiffre choisit par la machine
                Tant que chiffre pas trouvé     on continue 
                chiffre_trouve= False  
                
-R3: Demander à l'user de choisir un chiffre 
            Lui donner une réponse (trop grand ou trop petit)      
 

'''

'''
Correction Raffinage:
(Côté Machine User)

R0: Faire deviner un nombre à l'user

R1: Comment faire deviner un nombre:
        -Choisir un nombre
                solution: out
        -Faire deviner ce nombre:
                solution: in
        -Feliciter le joueur 
                nb_essai: in
                
R2: Comment "faire deviner ce nombre" ?
        nb_essai= 0
        trouve= False
        while trouve:
            -Demander la proposition de l'user
                proposition: out
            -Incrementer le nombre d'essais
                nb_essai: in/out
            -Donner un indice(Trop grand, Trop petit ou Trouvé)
                proposition: in
                solution: in
            
'''
from random import randint

def devin_user():
    '''
    Fait deviner à l'user un chiffre choisit par l'ordinateur compris entre 1 et 999.
    :return:None
    '''

    choix_machine= randint(1,999) #Choix d'un chiffre de la machine

    nb_essai= 0 #Nombre d'essais de l'user avant d'avoir trouvé le bon chiffre

    trouve= False

    while not trouve:

        choix_user= int(input("Veuillez choisir un chiffre compris entre 1 et 999: "))

        if choix_user == choix_machine:
            nb_essai += 1
            print("Félicitation vous avez mis", nb_essai, "essais pour trouver le bon chiffre.")
            trouve= True
        elif choix_user < choix_machine:
            nb_essai += 1
            print("Trop petit")
        elif choix_user > choix_machine:
            nb_essai += 1
            print("Trop Grand")








'''
(Côté User Machine)

-R0:Faire deviner à la machine un chiffre choisit par l'user compris entre 1 et 999.

-R1:Comment faire pour que l'user choisisse un chiffre compris entre 1 et 999 ?
            -l'user devra choisir un chiffre dans sa tête (1 à 999).
            

    Comment faire pour que l'ordinateur trouve le chiffre choisi par l'user ?
        -Proposition d'un chiffre de l'ordinateur compris entre 1 et 999. 
            -l'user rentre "p ou P" si le chiffre de l'ordinateur est plus petit que celui choisit par l'user.
                    -recherche dichotomique 
            -l'user rentre "g ou G" si le chiffre de l'ordinateur est plus grand que celui choisit par l'user.
                    -recherche dichotomique 
            -l'user rentre "t ou T" si le chiffre est trouvé.


    
-R2: Comment "Faire trouver ce nombre" par la machine ?
            a= 1
            b= 999
            propositionMachine= entre a et b
            trouve= False
            while trouve:
                -Afficher la proposition de la machine 
                        propositionMachine: out 
                -Réponse côté user "p ou P" "g ou G" "t ou T":
                -Si l'user entre 'p ou P'
                    dichotomie = (a + propositionMachine) // 2
                -Sinon si l'user entre 'g ou G'
                   dichotomie = (propositionMachine + b) // 2
                -Sinon si l'user entre 't ou T'
                -Félicitation l'ordinateur à trouver le bon chiffre
                    trouve= True



'''         


def devin_machine():
    '''
    Fait deviner à la machine un chiffre choisit par l'user compris entre 1 et 999.
    :return: None
    '''

    a= 1 

    b= 999

    proposition_machine= randint(a,b) #Proposition de départ
    print(proposition_machine)

    trouve= False

    while not trouve:
        
        reponse_user= input("Entrez 'p ou P' si plus petit ou 'g ou G' si plus grand ou 't ou T' si trouvé.")

        if reponse_user == 'p' or reponse_user == 'P':
            proposition_machine= (a + proposition_machine) // 2 #Calul Dichotomique 
            print(proposition_machine)

        elif reponse_user == 'g' or reponse_user == 'G':
            proposition_machine= (proposition_machine + b) // 2 #Calul Dichotomique
            print(proposition_machine)

        elif reponse_user == 't' or reponse_user == 'T':
            print("C'est le bon chiffre")
            trouve= True





def choix_jeu():
    '''
    Fait choisir auxquelles jeu souhaite jouer l'user, et recommencer à la fin d'un des jeux
    :return: None
    '''
    
    recommencer= True

    while recommencer:

        choix= (input("Veuillez choisir le jeu: 'devin user' ou 'devin machine' ?"))

        if choix == 'devin user':
            devin_user()
        elif choix == 'devin machine':
            devin_machine()

        reponse= input("Souhaitez-vous vraiment quitter ? si oui faites: 'o'")
                
        if reponse == 'o':
            recommencer= False




